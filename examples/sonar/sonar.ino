
#include "NewPing.h"

#define TRIGGER_PIN   12
#define ECHO_PIN      11
#define MAX_DISTANCE  150

#define LED_PIN       13
#define THRESHOLD     15

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

// the setup routine runs once when you press reset:
void setup() {             
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  
    int ping, distance;
    
    /* Do 5 pings, discard out of range pings and return median in microseconds  */
    ping = sonar.ping_median(5);
    
    /* Convert the reflection time to cm */
    distance = sonar.convert_cm(ping);
    
    Serial.print("Distance to nearest object: ");
    Serial.println(distance);  
    
    if(distance == 0){
        Serial.println("Too distant...");
        Serial.println(); /* empty line */
    }
    else{
        if(distance <= THRESHOLD){
          Serial.println("Close!");
          Serial.println(); /* empty line */
          digitalWrite(LED_PIN, HIGH);
        }
        else{
          Serial.println("Far...");
          Serial.println(); /* empty line */
          digitalWrite(LED_PIN, LOW);
        }
    }
    
} /* end of loop */
